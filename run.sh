#/usr/bin/env bash
set -e 

export FREESURFER_HOME='/Applications/freesurfer/7.1.1'
export CARET7DIR='/Applications/workbench/bin_macosx64'
export MSMBINDIR='/Users/seanf/scratch/ukbb-surf/msm'
export HCPPIPEDIR='/Users/seanf/dev/HCPpipelines-4.3.0'

datadir="/Users/seanf/scratch/ukbb-surf/Rsubjects6"
subid="1054688"

./ukbb_hcp_surface_mni.sh ${datadir} ${subid}
