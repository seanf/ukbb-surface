#!/bin/bash
# Copyright (c) 2011-2021 The Human Connectome Project and The Connectome Coordination Facility
set -e
# set -x

#  inputs

StudyFolder=$1
Subject=$2

# defaults

HCPPIPEDIR_Templates=${HCPPIPEDIR}/global/templates
HCPPIPEDIR_Config="${HCPPIPEDIR}/global/config"

SurfaceAtlasDIR="${HCPPIPEDIR_Templates}/standard_mesh_atlases"
GrayordinatesSpaceDIR="${HCPPIPEDIR_Templates}/91282_Greyordinates"

MSMCONFIGDIR="${HCPPIPEDIR}/MSMConfig"

SubcorticalGrayLabels="${HCPPIPEDIR_Config}/FreeSurferSubcorticalLabelTableLut.txt"
FreeSurferLabels="${HCPPIPEDIR_Config}/FreeSurferAllLut.txt"

GrayordinatesResolution="2" #Usually 2mm
HighResMesh="164"            #Usually 164k vertices
LowResMesh="32"              #Usually 32k vertices
# ReferenceMyelinMaps="${HCPPIPEDIR_Templates}/standard_mesh_atlases/Conte69.MyelinMap_BC.164k_fs_LR.dscalar.nii"
RegName="MSMSulc" #MSMSulc is recommended, if binary is not available use FS (FreeSurfer)

FinalfMRIResolution="2" #Needs to match what is in fMRIVolume, i.e. 2mm for 3T HCP data and 1.6mm for 7T HCP data

SmoothingFWHM="2"
Sigma=$(echo "$SmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l)

# UKBB files/directories

NameOffMRI="filtered_func_data_clean"
VolumefMRI="${StudyFolder}/${Subject}/fMRI/rfMRI.ica/reg_standard/${NameOffMRI}"
SBRef="${StudyFolder}/${Subject}/fMRI/rfMRI.ica/reg_standard/filtered_func_data_mean"

AtlasSpaceFolder="MNINonLinear"
T1wFolder="T1"
NativeFolder="Native"
ROIFolder="ROIs"

T1wImage="T1"
T1wImageBrainMask="T1_brain_mask"
AtlasSpaceT1wImage="T1"

AtlasSpaceFolder=${StudyFolder}/${Subject}/${AtlasSpaceFolder}
AtlasSpaceNativeFolder=${AtlasSpaceFolder}/${NativeFolder}

T1wFolder=${StudyFolder}/${Subject}/${T1wFolder}
ROIFolder=${AtlasSpaceFolder}/${ROIFolder}

FreeSurferFolder="${StudyFolder}/${Subject}/FreeSurfer"

AtlasTransform="${StudyFolder}/${Subject}/T1/transforms/T1_to_MNI_warp"
InverseAtlasTransform="${StudyFolder}/${Subject}/T1/transforms/MNI_to_T1_warp"

DownSampleFolder="fsaverage_LR${LowResMesh}k"
DownSampleFolder=${AtlasSpaceFolder}/${DownSampleFolder}

ResultsFolder="Results"
ResultsFolder=${AtlasSpaceFolder}/${ResultsFolder}/${NameOffMRI}

OutputAtlasDenseTimeseries=${ResultsFolder}/"${NameOffMRI}_Atlas"

WorkingDirectory="${StudyFolder}/${Subject}/RibbonVolumeToSurfaceMapping"

if [ ! -f "${InverseAtlasTransform}.nii.gz" ]; then
	echo "Invert atlas transform"
	invwarp -w $AtlasTransform -o $InverseAtlasTransform -r ${StudyFolder}/${Subject}/T1/T1.nii.gz
fi

# setup working directory

mkdir -p $AtlasSpaceNativeFolder
mkdir -p ${T1wFolder}/${NativeFolder}
mkdir -p ${AtlasSpaceFolder}/fsaverage
mkdir -p $ROIFolder
mkdir -p $ResultsFolder
mkdir -p ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k
mkdir -p ${T1wFolder}/fsaverage_LR${LowResMesh}k
# mkdir -p $(dirname ${VolumefMRI})

# ################
# convert RAS freesurfer surfaces to gifti and warp to MNI
# from PostFreeSurfer/scripts/FreeSurfer2CaretConvertAndRegisterNonlinear.sh
# ################

InflateExtraScale=1

# Find c_ras offset between FreeSurfer surface and volume and generate matrix to transform surfaces
# -- Corrected code using native mri_info --cras function to build the needed variables
MatrixXYZ=$(${FREESURFER_HOME}/bin/mri_info --cras ${FreeSurferFolder}/mri/brain.finalsurfs.mgz)
MatrixX=$(echo ${MatrixXYZ} | awk '{print $1;}')
MatrixY=$(echo ${MatrixXYZ} | awk '{print $2;}')
MatrixZ=$(echo ${MatrixXYZ} | awk '{print $3;}')
echo "1 0 0 ${MatrixX}" >${FreeSurferFolder}/mri/c_ras.mat
echo "0 1 0 ${MatrixY}" >>${FreeSurferFolder}/mri/c_ras.mat
echo "0 0 1 ${MatrixZ}" >>${FreeSurferFolder}/mri/c_ras.mat
echo "0 0 0 1" >>${FreeSurferFolder}/mri/c_ras.mat

#Convert FreeSurfer Volumes
for Image in wmparc aparc.a2009s+aseg aparc+aseg; do
	if [ -e ${FreeSurferFolder}/mri/${Image}.mgz ]; then
		${FREESURFER_HOME}/bin/mri_convert -rt nearest -rl ${T1wFolder}/${T1wImage}.nii.gz ${FreeSurferFolder}/mri/${Image}.mgz ${T1wFolder}/${Image}_1mm.nii.gz
		applywarp --rel --interp=nn -i ${T1wFolder}/${Image}_1mm.nii.gz -r ${T1wFolder}/${T1wImage}.nii.gz --premat=$FSLDIR/etc/flirtsch/ident.mat -o ${T1wFolder}/${Image}.nii.gz
		applywarp --rel --interp=nn -i ${T1wFolder}/${Image}_1mm.nii.gz -r $FSLDIR/data/standard/MNI152_T1_2mm.nii.gz -w ${AtlasTransform} -o ${AtlasSpaceFolder}/${Image}.nii.gz
		${CARET7DIR}/wb_command -volume-label-import ${T1wFolder}/${Image}.nii.gz ${FreeSurferLabels} ${T1wFolder}/${Image}.nii.gz -drop-unused-labels
		${CARET7DIR}/wb_command -volume-label-import ${AtlasSpaceFolder}/${Image}.nii.gz ${FreeSurferLabels} ${AtlasSpaceFolder}/${Image}.nii.gz -drop-unused-labels
	fi
done

# warp T1w and brain_mask to atlas space
applywarp --rel --interp=spline -i ${T1wFolder}/${T1wImage}.nii.gz -r $FSLDIR/data/standard/MNI152_T1_2mm.nii.gz -w ${AtlasTransform} -o ${AtlasSpaceFolder}/${AtlasSpaceT1wImage}.nii.gz
applywarp --rel --interp=nn -i ${T1wFolder}/${T1wImageBrainMask}.nii.gz -r $FSLDIR/data/standard/MNI152_T1_2mm.nii.gz -w ${AtlasTransform} -o ${AtlasSpaceFolder}/${T1wImageBrainMask}.nii.gz
fslmaths ${AtlasSpaceFolder}/${T1wImageBrainMask}.nii.gz -bin ${AtlasSpaceFolder}/${T1wImageBrainMask}.nii.gz

#Add volume files to spec files
${CARET7DIR}/wb_command -add-to-spec-file ${T1wFolder}/${NativeFolder}/${Subject}.native.wb.spec INVALID ${T1wFolder}/${T1wImage}.nii.gz
${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.native.wb.spec INVALID ${AtlasSpaceFolder}/${AtlasSpaceT1wImage}.nii.gz
${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${Subject}.${HighResMesh}k_fs_LR.wb.spec INVALID ${AtlasSpaceFolder}/${AtlasSpaceT1wImage}.nii.gz
${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec INVALID ${AtlasSpaceFolder}/${AtlasSpaceT1wImage}.nii.gz
${CARET7DIR}/wb_command -add-to-spec-file ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec INVALID ${T1wFolder}/${T1wImage}.nii.gz

#Import Subcortical ROIs
cp ${GrayordinatesSpaceDIR}/Atlas_ROIs.${GrayordinatesResolution}.nii.gz ${AtlasSpaceFolder}/ROIs/Atlas_ROIs.${GrayordinatesResolution}.nii.gz
applywarp --interp=nn -i ${AtlasSpaceFolder}/wmparc.nii.gz -r ${AtlasSpaceFolder}/ROIs/Atlas_ROIs.${GrayordinatesResolution}.nii.gz -o ${AtlasSpaceFolder}/ROIs/wmparc.${GrayordinatesResolution}.nii.gz
${CARET7DIR}/wb_command -volume-label-import ${AtlasSpaceFolder}/ROIs/wmparc.${GrayordinatesResolution}.nii.gz ${FreeSurferLabels} ${AtlasSpaceFolder}/ROIs/wmparc.${GrayordinatesResolution}.nii.gz -drop-unused-labels
applywarp --interp=nn -i ${SurfaceAtlasDIR}/Avgwmparc.nii.gz -r ${AtlasSpaceFolder}/ROIs/Atlas_ROIs.${GrayordinatesResolution}.nii.gz -o ${AtlasSpaceFolder}/ROIs/Atlas_wmparc.${GrayordinatesResolution}.nii.gz
${CARET7DIR}/wb_command -volume-label-import ${AtlasSpaceFolder}/ROIs/Atlas_wmparc.${GrayordinatesResolution}.nii.gz ${FreeSurferLabels} ${AtlasSpaceFolder}/ROIs/Atlas_wmparc.${GrayordinatesResolution}.nii.gz -drop-unused-labels
${CARET7DIR}/wb_command -volume-label-import ${AtlasSpaceFolder}/ROIs/wmparc.${GrayordinatesResolution}.nii.gz ${SubcorticalGrayLabels} ${AtlasSpaceFolder}/ROIs/ROIs.${GrayordinatesResolution}.nii.gz -discard-others
applywarp --interp=spline -i ${AtlasSpaceFolder}/${AtlasSpaceT1wImage}.nii.gz -r ${AtlasSpaceFolder}/ROIs/Atlas_ROIs.${GrayordinatesResolution}.nii.gz -o ${AtlasSpaceFolder}/${AtlasSpaceT1wImage}.${GrayordinatesResolution}.nii.gz

for Hemisphere in L R; do
	#Set a bunch of different ways of saying left and right
	if [ $Hemisphere = "L" ]; then
		hemisphere="l"
		Structure="CORTEX_LEFT"
	elif [ $Hemisphere = "R" ]; then
		hemisphere="r"
		Structure="CORTEX_RIGHT"
	fi

	#native Mesh Processing
	#Convert and volumetrically register white and pial surfaces makign linear and nonlinear copies, add each to the appropriate spec file
	Types="ANATOMICAL@GRAY_WHITE ANATOMICAL@PIAL"
	i=1
	for Surface in white pial; do
		Type=$(echo "$Types" | cut -d " " -f $i)
		Secondary=$(echo "$Type" | cut -d "@" -f 2)
		Type=$(echo "$Type" | cut -d "@" -f 1)
		if [ ! $Secondary = $Type ]; then
			Secondary=$(echo " -surface-secondary-type ""$Secondary")
		else
			Secondary=""
		fi
		${FREESURFER_HOME}/bin/mris_convert ${FreeSurferFolder}/surf/${hemisphere}h.${Surface} ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii
		${CARET7DIR}/wb_command -set-structure ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii ${Structure} -surface-type $Type$Secondary
		${CARET7DIR}/wb_command -surface-apply-affine ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii ${FreeSurferFolder}/mri/c_ras.mat ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii
		${CARET7DIR}/wb_command -add-to-spec-file ${T1wFolder}/${NativeFolder}/${Subject}.native.wb.spec $Structure ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii
		${CARET7DIR}/wb_command -surface-apply-warpfield ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii "$InverseAtlasTransform".nii.gz ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii -fnirt ${AtlasTransform}.nii.gz
		${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.native.wb.spec $Structure ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii
		i=$((i + 1))
	done

	#Create midthickness by averaging white and pial surfaces and use it to make inflated surfacess
	for Folder in ${T1wFolder} ${AtlasSpaceFolder}; do
		${CARET7DIR}/wb_command -surface-average ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii -surf ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.white.native.surf.gii -surf ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.pial.native.surf.gii
		${CARET7DIR}/wb_command -set-structure ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${Structure} -surface-type ANATOMICAL -surface-secondary-type MIDTHICKNESS
		${CARET7DIR}/wb_command -add-to-spec-file ${Folder}/${NativeFolder}/${Subject}.native.wb.spec $Structure ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii

		#get number of vertices from native file
		NativeVerts=$(${CARET7DIR}/wb_command -file-information ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii | grep 'Number of Vertices:' | cut -f2 -d: | tr -d '[:space:]')

		#HCP fsaverage_LR32k used -iterations-scale 0.75. Compute new param value for native mesh density
		NativeInflationScale=$(echo "scale=4; $InflateExtraScale * 0.75 * $NativeVerts / 32492" | bc -l)

		${CARET7DIR}/wb_command -surface-generate-inflated ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.inflated.native.surf.gii ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.very_inflated.native.surf.gii -iterations-scale $NativeInflationScale
		${CARET7DIR}/wb_command -add-to-spec-file ${Folder}/${NativeFolder}/${Subject}.native.wb.spec $Structure ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.inflated.native.surf.gii
		${CARET7DIR}/wb_command -add-to-spec-file ${Folder}/${NativeFolder}/${Subject}.native.wb.spec $Structure ${Folder}/${NativeFolder}/${Subject}.${Hemisphere}.very_inflated.native.surf.gii
	done

	#Convert original and registered spherical surfaces and add them to the nonlinear spec file
	for Surface in sphere.reg sphere; do
		${FREESURFER_HOME}/bin/mris_convert ${FreeSurferFolder}/surf/${hemisphere}h.${Surface} ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii
		${CARET7DIR}/wb_command -set-structure ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii ${Structure} -surface-type SPHERICAL
	done
	${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.native.wb.spec $Structure ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii

	#Add more files to the spec file and convert other FreeSurfer surface data to metric/GIFTI including sulc, curv, and thickness.
	for Map in sulc@sulc@Sulc thickness@thickness@Thickness curv@curvature@Curvature; do
		fsname=$(echo $Map | cut -d "@" -f 1)
		wbname=$(echo $Map | cut -d "@" -f 2)
		mapname=$(echo $Map | cut -d "@" -f 3)
		${FREESURFER_HOME}/bin/mris_convert -c ${FreeSurferFolder}/surf/${hemisphere}h.${fsname} ${FreeSurferFolder}/surf/${hemisphere}h.white ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${wbname}.native.shape.gii
		${CARET7DIR}/wb_command -set-structure ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${wbname}.native.shape.gii ${Structure}
		${CARET7DIR}/wb_command -metric-math "var * -1" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${wbname}.native.shape.gii -var var ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${wbname}.native.shape.gii
		${CARET7DIR}/wb_command -set-map-names ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${wbname}.native.shape.gii -map 1 ${Subject}_${Hemisphere}_"$mapname"
		${CARET7DIR}/wb_command -metric-palette ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${wbname}.native.shape.gii MODE_AUTO_SCALE_PERCENTAGE -pos-percent 2 98 -palette-name Gray_Interp -disp-pos true -disp-neg true -disp-zero true
	done
	#Thickness specific operations
	${CARET7DIR}/wb_command -metric-math "abs(thickness)" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii -var thickness ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii
	${CARET7DIR}/wb_command -metric-palette ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii MODE_AUTO_SCALE_PERCENTAGE -pos-percent 4 96 -interpolate true -palette-name videen_style -disp-pos true -disp-neg false -disp-zero false
	${CARET7DIR}/wb_command -metric-math "thickness > 0" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii -var thickness ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii
	${CARET7DIR}/wb_command -metric-fill-holes ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
	${CARET7DIR}/wb_command -metric-remove-islands ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii -map 1 ${Subject}_${Hemisphere}_ROI
	${CARET7DIR}/wb_command -metric-dilate ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii 10 ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii -nearest
	${CARET7DIR}/wb_command -metric-dilate ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.curvature.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii 10 ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.curvature.native.shape.gii -nearest

	#Label operations
	for Map in aparc aparc.a2009s; do #Remove BA because it doesn't convert properly
		if [ -e ${FreeSurferFolder}/label/${hemisphere}h.${Map}.annot ]; then
			mris_convert --annot ${FreeSurferFolder}/label/${hemisphere}h.${Map}.annot ${FreeSurferFolder}/surf/${hemisphere}h.white ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.label.gii
			${CARET7DIR}/wb_command -set-structure ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.label.gii $Structure
			${CARET7DIR}/wb_command -set-map-names ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.label.gii -map 1 ${Subject}_${Hemisphere}_${Map}
			${CARET7DIR}/wb_command -gifti-label-add-prefix ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.label.gii "${Hemisphere}_" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.label.gii
		fi
	done

	#Copy Atlas Files
	cp ${SurfaceAtlasDIR}/fs_${Hemisphere}/fsaverage.${Hemisphere}.sphere.${HighResMesh}k_fs_${Hemisphere}.surf.gii ${AtlasSpaceFolder}/fsaverage/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_${Hemisphere}.surf.gii
	cp ${SurfaceAtlasDIR}/fs_${Hemisphere}/fs_${Hemisphere}-to-fs_LR_fsaverage.${Hemisphere}_LR.spherical_std.${HighResMesh}k_fs_${Hemisphere}.surf.gii ${AtlasSpaceFolder}/fsaverage/${Subject}.${Hemisphere}.def_sphere.${HighResMesh}k_fs_${Hemisphere}.surf.gii
	cp ${SurfaceAtlasDIR}/fsaverage.${Hemisphere}_LR.spherical_std.${HighResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${Subject}.${HighResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii
	cp ${SurfaceAtlasDIR}/${Hemisphere}.atlasroi.${HighResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.atlasroi.${HighResMesh}k_fs_LR.shape.gii
	cp ${SurfaceAtlasDIR}/${Hemisphere}.refsulc.${HighResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.refsulc.${HighResMesh}k_fs_LR.shape.gii
	if [ -e ${SurfaceAtlasDIR}/colin.cerebral.${Hemisphere}.flat.${HighResMesh}k_fs_LR.surf.gii ]; then
		cp ${SurfaceAtlasDIR}/colin.cerebral.${Hemisphere}.flat.${HighResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.flat.${HighResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${Subject}.${HighResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.flat.${HighResMesh}k_fs_LR.surf.gii
	fi

	#Concatenate FS registration to FS --> FS_LR registration
	${CARET7DIR}/wb_command -surface-sphere-project-unproject ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.native.surf.gii ${AtlasSpaceFolder}/fsaverage/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_${Hemisphere}.surf.gii ${AtlasSpaceFolder}/fsaverage/${Subject}.${Hemisphere}.def_sphere.${HighResMesh}k_fs_${Hemisphere}.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.surf.gii

	#Make FreeSurfer Registration Areal Distortion Maps
	${CARET7DIR}/wb_command -surface-vertex-areas ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.shape.gii
	${CARET7DIR}/wb_command -surface-vertex-areas ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.shape.gii
	${CARET7DIR}/wb_command -metric-math "ln(spherereg / sphere) / ln(2)" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_FS.native.shape.gii -var sphere ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.shape.gii -var spherereg ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.shape.gii
	rm ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_FS.native.shape.gii -map 1 ${Subject}_${Hemisphere}_Areal_Distortion_FS
	${CARET7DIR}/wb_command -metric-palette ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_FS.native.shape.gii MODE_AUTO_SCALE -palette-name ROY-BIG-BL -thresholding THRESHOLD_TYPE_NORMAL THRESHOLD_TEST_SHOW_OUTSIDE -1 1

	${CARET7DIR}/wb_command -surface-distortion ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.EdgeDistortion_FS.native.shape.gii -edge-method

	${CARET7DIR}/wb_command -surface-distortion ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_FS.native.shape.gii -local-affine-method
	${CARET7DIR}/wb_command -metric-merge ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_FS.native.shape.gii -metric ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_FS.native.shape.gii -column 1
	${CARET7DIR}/wb_command -metric-merge ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_FS.native.shape.gii -metric ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_FS.native.shape.gii -column 2
	${CARET7DIR}/wb_command -metric-math "ln(var) / ln (2)" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_FS.native.shape.gii -var var ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_FS.native.shape.gii
	${CARET7DIR}/wb_command -metric-math "ln(var) / ln (2)" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_FS.native.shape.gii -var var ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_FS.native.shape.gii
	rm ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_FS.native.shape.gii

	#If desired, run MSMSulc folding-based registration to FS_LR initialized with FS affine
	if [ ${RegName} = "MSMSulc" ]; then
		#Calculate Affine Transform and Apply
		if [ ! -e ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc ]; then
			mkdir ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc
		fi
		${CARET7DIR}/wb_command -surface-affine-regression ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.mat
		${CARET7DIR}/wb_command -surface-apply-affine ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.mat ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.sphere_rot.surf.gii
		${CARET7DIR}/wb_command -surface-modify-sphere ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.sphere_rot.surf.gii 100 ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.sphere_rot.surf.gii
		cp ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.sphere_rot.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.rot.native.surf.gii
		DIR=$(pwd)
		cd ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc
		#Register using FreeSurfer Sulc Folding Map Using MSM Algorithm Configured for Reduced Distortion
		#${MSMBINDIR}/msm --version
		#${MSMBINDIR}/msm --levels=4 --conf=${MSMCONFIGDIR}/allparameterssulcDRconf --inmesh=${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.rot.native.surf.gii --trans=${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.rot.native.surf.gii --refmesh=${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii --indata=${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sulc.native.shape.gii --refdata=${AtlasSpaceFolder}/${Subject}.${Hemisphere}.refsulc.${HighResMesh}k_fs_LR.shape.gii --out=${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}. --verbose
		${MSMBINDIR}/msm --conf=${MSMCONFIGDIR}/MSMSulcStrainFinalconf --inmesh=${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.rot.native.surf.gii --refmesh=${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii --indata=${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sulc.native.shape.gii --refdata=${AtlasSpaceFolder}/${Subject}.${Hemisphere}.refsulc.${HighResMesh}k_fs_LR.shape.gii --out=${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}. --verbose
		cp ${MSMCONFIGDIR}/MSMSulcStrainFinalconf ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.logdir/conf
		cd $DIR
		#cp ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.HIGHRES_transformed.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.surf.gii
		cp ${AtlasSpaceFolder}/${NativeFolder}/MSMSulc/${Hemisphere}.sphere.reg.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.surf.gii
		${CARET7DIR}/wb_command -set-structure ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.surf.gii ${Structure}

		#Make MSMSulc Registration Areal Distortion Maps
		${CARET7DIR}/wb_command -surface-vertex-areas ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.shape.gii
		${CARET7DIR}/wb_command -surface-vertex-areas ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.shape.gii
		${CARET7DIR}/wb_command -metric-math "ln(spherereg / sphere) / ln(2)" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_MSMSulc.native.shape.gii -var sphere ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.shape.gii -var spherereg ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.shape.gii
		rm ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.shape.gii
		${CARET7DIR}/wb_command -set-map-names ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_MSMSulc.native.shape.gii -map 1 ${Subject}_${Hemisphere}_Areal_Distortion_MSMSulc
		${CARET7DIR}/wb_command -metric-palette ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_MSMSulc.native.shape.gii MODE_AUTO_SCALE -palette-name ROY-BIG-BL -thresholding THRESHOLD_TYPE_NORMAL THRESHOLD_TEST_SHOW_OUTSIDE -1 1

		${CARET7DIR}/wb_command -surface-distortion ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.EdgeDistortion_MSMSulc.native.shape.gii -edge-method

		${CARET7DIR}/wb_command -surface-distortion ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.surf.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_MSMSulc.native.shape.gii -local-affine-method
		${CARET7DIR}/wb_command -metric-merge ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_MSMSulc.native.shape.gii -metric ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_MSMSulc.native.shape.gii -column 1
		${CARET7DIR}/wb_command -metric-merge ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_MSMSulc.native.shape.gii -metric ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_MSMSulc.native.shape.gii -column 2
		${CARET7DIR}/wb_command -metric-math "ln(var) / ln (2)" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_MSMSulc.native.shape.gii -var var ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_MSMSulc.native.shape.gii
		${CARET7DIR}/wb_command -metric-math "ln(var) / ln (2)" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_MSMSulc.native.shape.gii -var var ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_MSMSulc.native.shape.gii
		rm ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.Strain_MSMSulc.native.shape.gii

		RegSphere="${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.MSMSulc.native.surf.gii"
	else
		RegSphere="${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sphere.reg.reg_LR.native.surf.gii"
	fi

	#Ensure no zeros in atlas medial wall ROI
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.atlasroi.${HighResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ${RegSphere} BARYCENTRIC ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.atlasroi.native.shape.gii -largest
	${CARET7DIR}/wb_command -metric-math "(atlas + individual) > 0" ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii -var atlas ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.atlasroi.native.shape.gii -var individual ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
	${CARET7DIR}/wb_command -metric-mask ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.thickness.native.shape.gii
	${CARET7DIR}/wb_command -metric-mask ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.curvature.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.curvature.native.shape.gii

	#Populate Highres fs_LR spec file.  Deform surfaces and other data according to native to folding-based registration selected above.  Regenerate inflated surfaces.
	for Surface in white midthickness pial; do
		${CARET7DIR}/wb_command -surface-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii BARYCENTRIC ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.${Surface}.${HighResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${Subject}.${HighResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.${Surface}.${HighResMesh}k_fs_LR.surf.gii
	done

	#HCP fsaverage_LR32k used -iterations-scale 0.75. Compute new param value for high res mesh density
	HighResInflationScale=$(echo "scale=4; $InflateExtraScale * 0.75 * $HighResMesh / 32" | bc -l)

	${CARET7DIR}/wb_command -surface-generate-inflated ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.inflated.${HighResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.very_inflated.${HighResMesh}k_fs_LR.surf.gii -iterations-scale $HighResInflationScale
	${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${Subject}.${HighResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.inflated.${HighResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/${Subject}.${HighResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.very_inflated.${HighResMesh}k_fs_LR.surf.gii

	for Map in thickness curvature; do
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.${Map}.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii -current-roi ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
		${CARET7DIR}/wb_command -metric-mask ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.${Map}.${HighResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.atlasroi.${HighResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.${Map}.${HighResMesh}k_fs_LR.shape.gii
	done
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.ArealDistortion_FS.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.EdgeDistortion_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.EdgeDistortion_FS.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.StrainJ_FS.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.StrainR_FS.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
	if [ ${RegName} = "MSMSulc" ]; then
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.ArealDistortion_MSMSulc.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.EdgeDistortion_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.EdgeDistortion_MSMSulc.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.StrainJ_MSMSulc.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.StrainR_MSMSulc.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii
	fi
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sulc.${HighResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.midthickness.${HighResMesh}k_fs_LR.surf.gii

	for Map in aparc aparc.a2009s; do #Remove BA because it doesn't convert properly
		if [ -e ${FreeSurferFolder}/label/${hemisphere}h.${Map}.annot ]; then
			${CARET7DIR}/wb_command -label-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.label.gii ${RegSphere} ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.sphere.${HighResMesh}k_fs_LR.surf.gii BARYCENTRIC ${AtlasSpaceFolder}/${Subject}.${Hemisphere}.${Map}.${HighResMesh}k_fs_LR.label.gii -largest
		fi
	done

	#Copy Atlas Files
	cp ${SurfaceAtlasDIR}/${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii
	cp ${GrayordinatesSpaceDIR}/${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii
	if [ -e ${SurfaceAtlasDIR}/colin.cerebral.${Hemisphere}.flat.${LowResMesh}k_fs_LR.surf.gii ]; then
		cp ${SurfaceAtlasDIR}/colin.cerebral.${Hemisphere}.flat.${LowResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.flat.${LowResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.flat.${LowResMesh}k_fs_LR.surf.gii
	fi

	#Create downsampled fs_LR spec files.
	for Surface in white midthickness pial; do
		${CARET7DIR}/wb_command -surface-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii BARYCENTRIC ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Surface}.${LowResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Surface}.${LowResMesh}k_fs_LR.surf.gii
	done

	#HCP fsaverage_LR32k used -iterations-scale 0.75. Recalculate in case using a different mesh
	LowResInflationScale=$(echo "scale=4; $InflateExtraScale * 0.75 * $LowResMesh / 32" | bc -l)

	${CARET7DIR}/wb_command -surface-generate-inflated ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.inflated.${LowResMesh}k_fs_LR.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.very_inflated.${LowResMesh}k_fs_LR.surf.gii -iterations-scale "$LowResInflationScale"
	${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.inflated.${LowResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -add-to-spec-file ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.very_inflated.${LowResMesh}k_fs_LR.surf.gii

	for Map in sulc thickness curvature; do
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Map}.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii -current-roi ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
		${CARET7DIR}/wb_command -metric-mask ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Map}.${LowResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Map}.${LowResMesh}k_fs_LR.shape.gii
	done
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.ArealDistortion_FS.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.EdgeDistortion_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.EdgeDistortion_FS.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.StrainJ_FS.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_FS.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.StrainR_FS.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
	if [ ${RegName} = "MSMSulc" ]; then
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.ArealDistortion_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.ArealDistortion_MSMSulc.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.EdgeDistortion_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.EdgeDistortion_MSMSulc.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainJ_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.StrainJ_MSMSulc.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.StrainR_MSMSulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.StrainR_MSMSulc.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
	fi
	${CARET7DIR}/wb_command -metric-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.sulc.native.shape.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sulc.${LowResMesh}k_fs_LR.shape.gii -area-surfs ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii

	for Map in aparc aparc.a2009s; do #Remove BA because it doesn't convert properly
		if [ -e ${FreeSurferFolder}/label/${hemisphere}h.${Map}.annot ]; then
			${CARET7DIR}/wb_command -label-resample ${AtlasSpaceFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Map}.native.label.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii BARYCENTRIC ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Map}.${LowResMesh}k_fs_LR.label.gii -largest
		fi
	done

	#Create downsampled fs_LR spec file in structural space.
	for Surface in white midthickness pial; do
		${CARET7DIR}/wb_command -surface-resample ${T1wFolder}/${NativeFolder}/${Subject}.${Hemisphere}.${Surface}.native.surf.gii ${RegSphere} ${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii BARYCENTRIC ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Surface}.${LowResMesh}k_fs_LR.surf.gii
		${CARET7DIR}/wb_command -add-to-spec-file ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.${Surface}.${LowResMesh}k_fs_LR.surf.gii
	done

	#HCP fsaverage_LR32k used -iterations-scale 0.75. Recalculate in case using a different mesh
	LowResInflationScale=$(echo "scale=4; $InflateExtraScale * 0.75 * $LowResMesh / 32" | bc -l)

	${CARET7DIR}/wb_command -surface-generate-inflated ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.inflated.${LowResMesh}k_fs_LR.surf.gii ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.very_inflated.${LowResMesh}k_fs_LR.surf.gii -iterations-scale "$LowResInflationScale"
	${CARET7DIR}/wb_command -add-to-spec-file ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.inflated.${LowResMesh}k_fs_LR.surf.gii
	${CARET7DIR}/wb_command -add-to-spec-file ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${LowResMesh}k_fs_LR.wb.spec $Structure ${T1wFolder}/fsaverage_LR${LowResMesh}k/${Subject}.${Hemisphere}.very_inflated.${LowResMesh}k_fs_LR.surf.gii

done

STRINGII=""
STRINGII=$(echo "${STRINGII}${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k@${LowResMesh}k_fs_LR@atlasroi ")

#Create CIFTI Files
for STRING in ${AtlasSpaceFolder}/${NativeFolder}@native@roi ${AtlasSpaceFolder}@${HighResMesh}k_fs_LR@atlasroi ${STRINGII}; do
	Folder=$(echo $STRING | cut -d "@" -f 1)
	Mesh=$(echo $STRING | cut -d "@" -f 2)
	ROI=$(echo $STRING | cut -d "@" -f 3)

	${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.sulc.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.sulc.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.sulc.${Mesh}.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.sulc.${Mesh}.dscalar.nii -map 1 "${Subject}_Sulc"
	${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.sulc.${Mesh}.dscalar.nii MODE_AUTO_SCALE_PERCENTAGE ${Folder}/${Subject}.sulc.${Mesh}.dscalar.nii -pos-percent 2 98 -palette-name Gray_Interp -disp-pos true -disp-neg true -disp-zero true

	${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.curvature.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.curvature.${Mesh}.shape.gii -roi-left ${Folder}/${Subject}.L.${ROI}.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.curvature.${Mesh}.shape.gii -roi-right ${Folder}/${Subject}.R.${ROI}.${Mesh}.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.curvature.${Mesh}.dscalar.nii -map 1 "${Subject}_Curvature"
	${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.curvature.${Mesh}.dscalar.nii MODE_AUTO_SCALE_PERCENTAGE ${Folder}/${Subject}.curvature.${Mesh}.dscalar.nii -pos-percent 2 98 -palette-name Gray_Interp -disp-pos true -disp-neg true -disp-zero true

	${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.thickness.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.thickness.${Mesh}.shape.gii -roi-left ${Folder}/${Subject}.L.${ROI}.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.thickness.${Mesh}.shape.gii -roi-right ${Folder}/${Subject}.R.${ROI}.${Mesh}.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.thickness.${Mesh}.dscalar.nii -map 1 "${Subject}_Thickness"
	${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.thickness.${Mesh}.dscalar.nii MODE_AUTO_SCALE_PERCENTAGE ${Folder}/${Subject}.thickness.${Mesh}.dscalar.nii -pos-percent 4 96 -interpolate true -palette-name videen_style -disp-pos true -disp-neg false -disp-zero false

	${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.ArealDistortion_FS.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.ArealDistortion_FS.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.ArealDistortion_FS.${Mesh}.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.ArealDistortion_FS.${Mesh}.dscalar.nii -map 1 "${Subject}_ArealDistortion_FS"
	${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.ArealDistortion_FS.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.ArealDistortion_FS.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false

	${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.EdgeDistortion_FS.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.EdgeDistortion_FS.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.EdgeDistortion_FS.${Mesh}.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.EdgeDistortion_FS.${Mesh}.dscalar.nii -map 1 "${Subject}_EdgeDistortion_FS"
	${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.EdgeDistortion_FS.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.EdgeDistortion_FS.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false

	${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.StrainJ_FS.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.StrainJ_FS.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.StrainJ_FS.${Mesh}.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.StrainJ_FS.${Mesh}.dscalar.nii -map 1 "${Subject}_StrainJ_FS"
	${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.StrainJ_FS.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.StrainJ_FS.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false

	${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.StrainR_FS.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.StrainR_FS.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.StrainR_FS.${Mesh}.shape.gii
	${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.StrainR_FS.${Mesh}.dscalar.nii -map 1 "${Subject}_StrainR_FS"
	${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.StrainR_FS.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.StrainR_FS.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false

	if [ ${RegName} = "MSMSulc" ]; then
		${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.ArealDistortion_MSMSulc.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.ArealDistortion_MSMSulc.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.ArealDistortion_MSMSulc.${Mesh}.shape.gii
		${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.ArealDistortion_MSMSulc.${Mesh}.dscalar.nii -map 1 "${Subject}_ArealDistortion_MSMSulc"
		${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.ArealDistortion_MSMSulc.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.ArealDistortion_MSMSulc.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false

		${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.EdgeDistortion_MSMSulc.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.EdgeDistortion_MSMSulc.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.EdgeDistortion_MSMSulc.${Mesh}.shape.gii
		${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.EdgeDistortion_MSMSulc.${Mesh}.dscalar.nii -map 1 "${Subject}_EdgeDistortion_MSMSulc"
		${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.EdgeDistortion_MSMSulc.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.EdgeDistortion_MSMSulc.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false

		${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.StrainJ_MSMSulc.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.StrainJ_MSMSulc.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.StrainJ_MSMSulc.${Mesh}.shape.gii
		${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.StrainJ_MSMSulc.${Mesh}.dscalar.nii -map 1 "${Subject}_StrainJ_MSMSulc"
		${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.StrainJ_MSMSulc.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.StrainJ_MSMSulc.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false

		${CARET7DIR}/wb_command -cifti-create-dense-scalar ${Folder}/${Subject}.StrainR_MSMSulc.${Mesh}.dscalar.nii -left-metric ${Folder}/${Subject}.L.StrainR_MSMSulc.${Mesh}.shape.gii -right-metric ${Folder}/${Subject}.R.StrainR_MSMSulc.${Mesh}.shape.gii
		${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.StrainR_MSMSulc.${Mesh}.dscalar.nii -map 1 "${Subject}_StrainR_MSMSulc"
		${CARET7DIR}/wb_command -cifti-palette ${Folder}/${Subject}.StrainR_MSMSulc.${Mesh}.dscalar.nii MODE_USER_SCALE ${Folder}/${Subject}.StrainR_MSMSulc.${Mesh}.dscalar.nii -pos-user 0 1 -neg-user 0 -1 -interpolate true -palette-name ROY-BIG-BL -disp-pos true -disp-neg true -disp-zero false
	fi

	for Map in aparc aparc.a2009s; do #Remove BA because it doesn't convert properly
		if [ -e ${Folder}/${Subject}.L.${Map}.${Mesh}.label.gii ]; then
			${CARET7DIR}/wb_command -cifti-create-label ${Folder}/${Subject}.${Map}.${Mesh}.dlabel.nii -left-label ${Folder}/${Subject}.L.${Map}.${Mesh}.label.gii -roi-left ${Folder}/${Subject}.L.${ROI}.${Mesh}.shape.gii -right-label ${Folder}/${Subject}.R.${Map}.${Mesh}.label.gii -roi-right ${Folder}/${Subject}.R.${ROI}.${Mesh}.shape.gii
			${CARET7DIR}/wb_command -set-map-names ${Folder}/${Subject}.${Map}.${Mesh}.dlabel.nii -map 1 ${Subject}_${Map}
		fi
	done
done

STRINGII=""
STRINGII=$(echo "${STRINGII}${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k@${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k@${LowResMesh}k_fs_LR ${T1wFolder}/fsaverage_LR${LowResMesh}k@${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k@${LowResMesh}k_fs_LR ")

#Add CIFTI Maps to Spec Files
for STRING in ${T1wFolder}/${NativeFolder}@${AtlasSpaceFolder}/${NativeFolder}@native ${AtlasSpaceFolder}/${NativeFolder}@${AtlasSpaceFolder}/${NativeFolder}@native ${AtlasSpaceFolder}@${AtlasSpaceFolder}@${HighResMesh}k_fs_LR ${STRINGII}; do
	FolderI=$(echo $STRING | cut -d "@" -f 1)
	FolderII=$(echo $STRING | cut -d "@" -f 2)
	Mesh=$(echo $STRING | cut -d "@" -f 3)
	for STRINGII in sulc@dscalar thickness@dscalar curvature@dscalar aparc@dlabel aparc.a2009s@dlabel; do #Remove BA@dlabel because it doesn't convert properly
		Map=$(echo $STRINGII | cut -d "@" -f 1)
		Ext=$(echo $STRINGII | cut -d "@" -f 2)
		if [ -e ${FolderII}/${Subject}.${Map}.${Mesh}.${Ext}.nii ]; then
			${CARET7DIR}/wb_command -add-to-spec-file "$FolderI"/${Subject}.${Mesh}.wb.spec INVALID ${FolderII}/${Subject}.${Map}.${Mesh}.${Ext}.nii
		fi
	done
done

# Create midthickness Vertex Area (VA) maps
echo "Create midthickness Vertex Area (VA) maps"
echo "Creating midthickness Vertex Area (VA) maps for LowResMesh: ${LowResMesh}"

# DownSampleT1wFolder             - path to folder containing downsampled T1w files
# midthickness_va_file            - path to non-normalized midthickness vertex area file
# normalized_midthickness_va_file - path ot normalized midthickness vertex area file
# surface_to_measure              - path to surface file on which to measure surface areas
# output_metric                   - path to metric file generated by -surface-vertex-areas subcommand

DownSampleT1wFolder=${T1wFolder}/fsaverage_LR${LowResMesh}k
DownSampleFolder=${AtlasSpaceFolder}/fsaverage_LR${LowResMesh}k
midthickness_va_file=${DownSampleT1wFolder}/${Subject}.midthickness_va.${LowResMesh}k_fs_LR.dscalar.nii
normalized_midthickness_va_file=${DownSampleT1wFolder}/${Subject}.midthickness_va_norm.${LowResMesh}k_fs_LR.dscalar.nii

for Hemisphere in L R; do
	surface_to_measure=${DownSampleT1wFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii
	output_metric=${DownSampleT1wFolder}/${Subject}.${Hemisphere}.midthickness_va.${LowResMesh}k_fs_LR.shape.gii
	${CARET7DIR}/wb_command -surface-vertex-areas ${surface_to_measure} ${output_metric}
done

# left_metric  - path to left hemisphere VA metric file
# roi_left     - path to file of ROI vertices to use from left surface
# right_metric - path to right hemisphere VA metric file
# roi_right    - path to file of ROI vertices to use from right surface

left_metric=${DownSampleT1wFolder}/${Subject}.L.midthickness_va.${LowResMesh}k_fs_LR.shape.gii
roi_left=${DownSampleFolder}/${Subject}.L.atlasroi.${LowResMesh}k_fs_LR.shape.gii
right_metric=${DownSampleT1wFolder}/${Subject}.R.midthickness_va.${LowResMesh}k_fs_LR.shape.gii
roi_right=${DownSampleFolder}/${Subject}.R.atlasroi.${LowResMesh}k_fs_LR.shape.gii

${CARET7DIR}/wb_command -cifti-create-dense-scalar ${midthickness_va_file} \
	-left-metric ${left_metric} \
	-roi-left ${roi_left} \
	-right-metric ${right_metric} \
	-roi-right ${roi_right}

# VAMean - mean of surface area accounted for for each vertex - used for normalization
VAMean=$(${CARET7DIR}/wb_command -cifti-stats ${midthickness_va_file} -reduce MEAN)
echo "VAMean: ${VAMean}"

${CARET7DIR}/wb_command -cifti-math "VA / ${VAMean}" ${normalized_midthickness_va_file} -var VA ${midthickness_va_file}

echo "Done creating midthickness Vertex Area (VA) maps for LowResMesh: ${LowResMesh}"

# ################
# ribbon constrained mapping
# from fMRISurface/scripts/RibbonVolumeToSurfaceMapping.sh
# ################

if [ ${RegName} = "FS" ]; then
	RegName="reg.reg_LR"
fi

LeftGreyRibbonValue="1"
RightGreyRibbonValue="1"

NeighborhoodSmoothing="5"
Factor="0.5"

fslmaths $VolumefMRI -Tmean $SBRef

mkdir -p $WorkingDirectory

for Hemisphere in L R; do
	if [ $Hemisphere = "L" ]; then
		GreyRibbonValue="$LeftGreyRibbonValue"
	elif [ $Hemisphere = "R" ]; then
		GreyRibbonValue="$RightGreyRibbonValue"
	fi
	${CARET7DIR}/wb_command -create-signed-distance-volume ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.white.native.surf.gii ${SBRef}.nii.gz ${WorkingDirectory}/${Subject}.${Hemisphere}.white.native.nii.gz
	${CARET7DIR}/wb_command -create-signed-distance-volume ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.pial.native.surf.gii ${SBRef}.nii.gz ${WorkingDirectory}/${Subject}.${Hemisphere}.pial.native.nii.gz
	fslmaths ${WorkingDirectory}/${Subject}.${Hemisphere}.white.native.nii.gz -thr 0 -bin -mul 255 ${WorkingDirectory}/${Subject}.${Hemisphere}.white_thr0.native.nii.gz
	fslmaths ${WorkingDirectory}/${Subject}.${Hemisphere}.white_thr0.native.nii.gz -bin ${WorkingDirectory}/${Subject}.${Hemisphere}.white_thr0.native.nii.gz
	fslmaths ${WorkingDirectory}/${Subject}.${Hemisphere}.pial.native.nii.gz -uthr 0 -abs -bin -mul 255 ${WorkingDirectory}/${Subject}.${Hemisphere}.pial_uthr0.native.nii.gz
	fslmaths ${WorkingDirectory}/${Subject}.${Hemisphere}.pial_uthr0.native.nii.gz -bin ${WorkingDirectory}/${Subject}.${Hemisphere}.pial_uthr0.native.nii.gz
	fslmaths ${WorkingDirectory}/${Subject}.${Hemisphere}.pial_uthr0.native.nii.gz -mas ${WorkingDirectory}/${Subject}.${Hemisphere}.white_thr0.native.nii.gz -mul 255 ${WorkingDirectory}/${Subject}.${Hemisphere}.ribbon.nii.gz
	fslmaths ${WorkingDirectory}/${Subject}.${Hemisphere}.ribbon.nii.gz -bin -mul $GreyRibbonValue ${WorkingDirectory}/${Subject}.${Hemisphere}.ribbon.nii.gz
	rm ${WorkingDirectory}/${Subject}.${Hemisphere}.white.native.nii.gz ${WorkingDirectory}/${Subject}.${Hemisphere}.white_thr0.native.nii.gz ${WorkingDirectory}/${Subject}.${Hemisphere}.pial.native.nii.gz ${WorkingDirectory}/${Subject}.${Hemisphere}.pial_uthr0.native.nii.gz
done

fslmaths ${WorkingDirectory}/${Subject}.L.ribbon.nii.gz -add ${WorkingDirectory}/${Subject}.R.ribbon.nii.gz ${WorkingDirectory}/ribbon_only.nii.gz
rm ${WorkingDirectory}/${Subject}.L.ribbon.nii.gz ${WorkingDirectory}/${Subject}.R.ribbon.nii.gz

fslmaths ${VolumefMRI} -Tmean ${WorkingDirectory}/mean -odt float
fslmaths ${VolumefMRI} -Tstd ${WorkingDirectory}/std -odt float
fslmaths ${WorkingDirectory}/std -div ${WorkingDirectory}/mean ${WorkingDirectory}/cov

fslmaths ${WorkingDirectory}/cov -mas ${WorkingDirectory}/ribbon_only.nii.gz ${WorkingDirectory}/cov_ribbon

fslmaths ${WorkingDirectory}/cov_ribbon -div $(fslstats ${WorkingDirectory}/cov_ribbon -M) ${WorkingDirectory}/cov_ribbon_norm
fslmaths ${WorkingDirectory}/cov_ribbon_norm -bin -s $NeighborhoodSmoothing ${WorkingDirectory}/SmoothNorm
fslmaths ${WorkingDirectory}/cov_ribbon_norm -s $NeighborhoodSmoothing -div ${WorkingDirectory}/SmoothNorm -dilD ${WorkingDirectory}/cov_ribbon_norm_s$NeighborhoodSmoothing
fslmaths ${WorkingDirectory}/cov -div $(fslstats ${WorkingDirectory}/cov_ribbon -M) -div ${WorkingDirectory}/cov_ribbon_norm_s$NeighborhoodSmoothing ${WorkingDirectory}/cov_norm_modulate
fslmaths ${WorkingDirectory}/cov_norm_modulate -mas ${WorkingDirectory}/ribbon_only.nii.gz ${WorkingDirectory}/cov_norm_modulate_ribbon

STD=$(fslstats ${WorkingDirectory}/cov_norm_modulate_ribbon -S)
echo $STD
MEAN=$(fslstats ${WorkingDirectory}/cov_norm_modulate_ribbon -M)
echo $MEAN
Lower=$(echo "$MEAN - ($STD * $Factor)" | bc -l)
echo $Lower
Upper=$(echo "$MEAN + ($STD * $Factor)" | bc -l)
echo $Upper

fslmaths ${WorkingDirectory}/mean -bin ${WorkingDirectory}/mask
fslmaths ${WorkingDirectory}/cov_norm_modulate -thr $Upper -bin -sub ${WorkingDirectory}/mask -mul -1 ${WorkingDirectory}/goodvoxels

for Hemisphere in L R; do
	for Map in mean cov; do
		${CARET7DIR}/wb_command -volume-to-surface-mapping ${WorkingDirectory}/${Map}.nii.gz ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${WorkingDirectory}/${Hemisphere}.${Map}.native.func.gii -ribbon-constrained ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.white.native.surf.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.pial.native.surf.gii -volume-roi ${WorkingDirectory}/goodvoxels.nii.gz
		${CARET7DIR}/wb_command -metric-dilate ${WorkingDirectory}/${Hemisphere}.${Map}.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii 10 ${WorkingDirectory}/${Hemisphere}.${Map}.native.func.gii -nearest
		${CARET7DIR}/wb_command -metric-mask ${WorkingDirectory}/${Hemisphere}.${Map}.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${WorkingDirectory}/${Hemisphere}.${Map}.native.func.gii
		${CARET7DIR}/wb_command -volume-to-surface-mapping ${WorkingDirectory}/${Map}.nii.gz ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${WorkingDirectory}/${Hemisphere}.${Map}_all.native.func.gii -ribbon-constrained ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.white.native.surf.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.pial.native.surf.gii
		${CARET7DIR}/wb_command -metric-mask ${WorkingDirectory}/${Hemisphere}.${Map}_all.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${WorkingDirectory}/${Hemisphere}.${Map}_all.native.func.gii
		${CARET7DIR}/wb_command -metric-resample ${WorkingDirectory}/${Hemisphere}.${Map}.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.sphere.${RegName}.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${WorkingDirectory}/${Hemisphere}.${Map}.${LowResMesh}k_fs_LR.func.gii -area-surfs ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii -current-roi ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
		${CARET7DIR}/wb_command -metric-mask ${WorkingDirectory}/${Hemisphere}.${Map}.${LowResMesh}k_fs_LR.func.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii ${WorkingDirectory}/${Hemisphere}.${Map}.${LowResMesh}k_fs_LR.func.gii
		${CARET7DIR}/wb_command -metric-resample ${WorkingDirectory}/${Hemisphere}.${Map}_all.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.sphere.${RegName}.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${WorkingDirectory}/${Hemisphere}.${Map}_all.${LowResMesh}k_fs_LR.func.gii -area-surfs ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii -current-roi ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
		${CARET7DIR}/wb_command -metric-mask ${WorkingDirectory}/${Hemisphere}.${Map}_all.${LowResMesh}k_fs_LR.func.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii ${WorkingDirectory}/${Hemisphere}.${Map}_all.${LowResMesh}k_fs_LR.func.gii
	done
	${CARET7DIR}/wb_command -volume-to-surface-mapping ${WorkingDirectory}/goodvoxels.nii.gz ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${WorkingDirectory}/${Hemisphere}.goodvoxels.native.func.gii -ribbon-constrained ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.white.native.surf.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.pial.native.surf.gii
	${CARET7DIR}/wb_command -metric-mask ${WorkingDirectory}/${Hemisphere}.goodvoxels.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${WorkingDirectory}/${Hemisphere}.goodvoxels.native.func.gii
	${CARET7DIR}/wb_command -metric-resample ${WorkingDirectory}/${Hemisphere}.goodvoxels.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.sphere.${RegName}.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${WorkingDirectory}/${Hemisphere}.goodvoxels.${LowResMesh}k_fs_LR.func.gii -area-surfs ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii -current-roi ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
	${CARET7DIR}/wb_command -metric-mask ${WorkingDirectory}/${Hemisphere}.goodvoxels.${LowResMesh}k_fs_LR.func.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii ${WorkingDirectory}/${Hemisphere}.goodvoxels.${LowResMesh}k_fs_LR.func.gii

	${CARET7DIR}/wb_command -volume-to-surface-mapping ${VolumefMRI}.nii.gz ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${VolumefMRI}.${Hemisphere}.native.func.gii -ribbon-constrained ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.white.native.surf.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.pial.native.surf.gii -volume-roi ${WorkingDirectory}/goodvoxels.nii.gz
	${CARET7DIR}/wb_command -metric-dilate ${VolumefMRI}.${Hemisphere}.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii 10 ${VolumefMRI}.${Hemisphere}.native.func.gii -nearest
	${CARET7DIR}/wb_command -metric-mask ${VolumefMRI}.${Hemisphere}.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii ${VolumefMRI}.${Hemisphere}.native.func.gii

	echo ${CARET7DIR}/wb_command -metric-resample ${VolumefMRI}.${Hemisphere}.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.sphere.${RegName}.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii -area-surfs ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii -current-roi ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
	echo ${CARET7DIR}/wb_command -metric-dilate ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii 30 ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii -nearest
	echo ${CARET7DIR}/wb_command -metric-mask ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii

	${CARET7DIR}/wb_command -metric-resample ${VolumefMRI}.${Hemisphere}.native.func.gii ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.sphere.${RegName}.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.sphere.${LowResMesh}k_fs_LR.surf.gii ADAP_BARY_AREA ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii -area-surfs ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.midthickness.native.surf.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii -current-roi ${AtlasSpaceNativeFolder}/${Subject}.${Hemisphere}.roi.native.shape.gii
	${CARET7DIR}/wb_command -metric-dilate ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii 30 ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii -nearest
	${CARET7DIR}/wb_command -metric-mask ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii ${DownSampleFolder}/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii
done

# ################
# surface smoothing
# from fMRISurface/scripts/SurfaceSmoothing.sh
# ################

for Hemisphere in L R; do
	${CARET7DIR}/wb_command -metric-smoothing ${DownSampleFolder}/${Subject}.${Hemisphere}.midthickness.${LowResMesh}k_fs_LR.surf.gii ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii "$Sigma" ${VolumefMRI}_s${SmoothingFWHM}.atlasroi.${Hemisphere}.${LowResMesh}k_fs_LR.func.gii -roi ${DownSampleFolder}/${Subject}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.shape.gii
	#Basic Cleanup
	rm ${VolumefMRI}.${Hemisphere}.atlasroi.${LowResMesh}k_fs_LR.func.gii
done

# ################
# subcortical processing
# from fMRISurface/scripts/SubcorticalProcessing.sh
# ################

#deal with fsl_sub being silly when we want to use numeric equality on decimals
unset POSIXLY_CORRECT

#generate subject-roi space fMRI cifti for subcortical
if [[ $(echo "$GrayordinatesResolution == $FinalfMRIResolution" | bc -l | cut -f1 -d.) == "1" ]]; then
	echo "Creating subject-roi subcortical cifti at same resolution as output"
	${CARET7DIR}/wb_command -cifti-create-dense-timeseries ${ResultsFolder}/${NameOffMRI}_temp_subject.dtseries.nii -volume ${VolumefMRI}.nii.gz ${ROIFolder}/ROIs.${GrayordinatesResolution}.nii.gz
else
	echo "Creating subject-roi subcortical cifti at differing fMRI resolution"
	${CARET7DIR}/wb_command -volume-affine-resample ${ROIFolder}/ROIs.${GrayordinatesResolution}.nii.gz $FSLDIR/etc/flirtsch/ident.mat ${VolumefMRI}.nii.gz ENCLOSING_VOXEL ${ResultsFolder}/ROIs."$FinalfMRIResolution".nii.gz
	${CARET7DIR}/wb_command -cifti-create-dense-timeseries ${ResultsFolder}/${NameOffMRI}_temp_subject.dtseries.nii -volume ${VolumefMRI}.nii.gz ${ResultsFolder}/ROIs."$FinalfMRIResolution".nii.gz
	rm -f ${ResultsFolder}/ROIs."$FinalfMRIResolution".nii.gz
fi

echo "Dilating out zeros"
#dilate out any exact zeros in the input data, for instance if the brain mask is wrong. Note that the CIFTI space cannot contain zeros to produce a valid CIFTI file (dilation also occurs below).
${CARET7DIR}/wb_command -cifti-dilate ${ResultsFolder}/${NameOffMRI}_temp_subject.dtseries.nii COLUMN 0 30 ${ResultsFolder}/${NameOffMRI}_temp_subject_dilate.dtseries.nii
rm -f ${ResultsFolder}/${NameOffMRI}_temp_subject.dtseries.nii

# echo "Generate atlas subcortical template cifti"
${CARET7DIR}/wb_command -cifti-create-label ${ResultsFolder}/${NameOffMRI}_temp_template.dlabel.nii -volume ${ROIFolder}/Atlas_ROIs.${GrayordinatesResolution}.nii.gz ${ROIFolder}/Atlas_ROIs.${GrayordinatesResolution}.nii.gz

#As of wb_command 1.4.0 and later, volume predilate is much less important for reducing edge ringing, and could be reduced
if [[ $(echo "${Sigma} > 0" | bc -l | cut -f1 -d.) == "1" ]]; then
	echo "Smoothing"
	#this is the whole timeseries, so don't overwrite, in order to allow on-disk writing, then delete temporary
	${CARET7DIR}/wb_command -cifti-smoothing ${ResultsFolder}/${NameOffMRI}_temp_subject_dilate.dtseries.nii 0 ${Sigma} COLUMN ${ResultsFolder}/${NameOffMRI}_temp_subject_smooth.dtseries.nii -fix-zeros-volume
	#resample, delete temporary
	echo "Resampling"
	${CARET7DIR}/wb_command -cifti-resample ${ResultsFolder}/${NameOffMRI}_temp_subject_smooth.dtseries.nii COLUMN ${ResultsFolder}/${NameOffMRI}_temp_template.dlabel.nii COLUMN ADAP_BARY_AREA CUBIC ${ResultsFolder}/${NameOffMRI}_temp_atlas.dtseries.nii -volume-predilate 10
	rm -f ${ResultsFolder}/${NameOffMRI}_temp_subject_smooth.dtseries.nii
else
	echo "Resampling"
	${CARET7DIR}/wb_command -cifti-resample ${ResultsFolder}/${NameOffMRI}_temp_subject_dilate.dtseries.nii COLUMN ${ResultsFolder}/${NameOffMRI}_temp_template.dlabel.nii COLUMN ADAP_BARY_AREA CUBIC ${ResultsFolder}/${NameOffMRI}_temp_atlas.dtseries.nii -volume-predilate 10
fi

#delete common temporaries
rm -f ${ResultsFolder}/${NameOffMRI}_temp_subject_dilate.dtseries.nii
rm -f ${ResultsFolder}/${NameOffMRI}_temp_template.dlabel.nii

#the standard space output cifti must not contain zeros (or correlation, ICA, variance normalization, etc will break), so dilate in case freesurfer was unable to segment something (may only be applicable for bad quality structurals)
#NOTE: wb_command v1.4.0 and later should only output exact 0s past the edge of predilate, so this works as desired
#earlier verions of wb_command may produce undesired results in the subjects that need this dilation
${CARET7DIR}/wb_command -cifti-dilate ${ResultsFolder}/${NameOffMRI}_temp_atlas.dtseries.nii COLUMN 0 30 ${ResultsFolder}/${NameOffMRI}_temp_atlas_dilate.dtseries.nii
rm -f ${ResultsFolder}/${NameOffMRI}_temp_atlas.dtseries.nii

#write output volume, delete temporary
#NOTE: $VolumefMRI contains a path in it, it is not a file in the current directory
${CARET7DIR}/wb_command -cifti-separate ${ResultsFolder}/${NameOffMRI}_temp_atlas_dilate.dtseries.nii COLUMN -volume-all ${VolumefMRI}_AtlasSubcortical_s${SmoothingFWHM}.nii.gz
rm -f ${ResultsFolder}/${NameOffMRI}_temp_atlas_dilate.dtseries.nii

# ################
# create CIFTI dtseries
# from fMRISurface/scripts/CreateDenseTimeseries.sh
# ################

TR_vol=$(fslval ${VolumefMRI} pixdim4 | cut -d " " -f 1)

#Some way faster and more concise code:

${CARET7DIR}/wb_command -cifti-create-dense-timeseries \
	${OutputAtlasDenseTimeseries}.dtseries.nii \
	-volume ${VolumefMRI}_AtlasSubcortical_s${SmoothingFWHM}.nii.gz ${ROIFolder}/Atlas_ROIs.${GrayordinatesResolution}.nii.gz \
	-left-metric ${VolumefMRI}_s${SmoothingFWHM}.atlasroi.L.${LowResMesh}k_fs_LR.func.gii -roi-left ${DownSampleFolder}/${Subject}.L.atlasroi.${LowResMesh}k_fs_LR.shape.gii \
	-right-metric ${VolumefMRI}_s${SmoothingFWHM}.atlasroi.R.${LowResMesh}k_fs_LR.func.gii -roi-right ${DownSampleFolder}/${Subject}.R.atlasroi.${LowResMesh}k_fs_LR.shape.gii \
	-timestep "$TR_vol"

# Generate temporal mean of timeseries, for display in fMRIQC
${CARET7DIR}/wb_command -cifti-reduce ${OutputAtlasDenseTimeseries}.dtseries.nii MEAN ${OutputAtlasDenseTimeseries}_mean.dscalar.nii

#Assess for zeros in the final dtseries (e.g., due to incomplete spatial coverage)
# (Note that earlier steps do an appreciable amount of dilation to eliminate zeros,
# so zeros remaining in the CIFTI at this point represent a non-trivial issue with spatial coverage)
${CARET7DIR}/wb_command -cifti-reduce ${OutputAtlasDenseTimeseries}.dtseries.nii STDEV ${OutputAtlasDenseTimeseries}.stdev.dscalar.nii
Nnonzero=$(${CARET7DIR}/wb_command -cifti-stats ${OutputAtlasDenseTimeseries}.stdev.dscalar.nii -reduce COUNT_NONZERO)
Ngrayordinates=$(${CARET7DIR}/wb_command -file-information ${OutputAtlasDenseTimeseries}.stdev.dscalar.nii | grep "Number of Rows" | awk '{print $4}')
PctCoverage=$(echo "scale=4; 100 * ${Nnonzero} / ${Ngrayordinates}" | bc -l)
echo "PctCoverage, Nnonzero, Ngrayordinates" >|${OutputAtlasDenseTimeseries}_nonzero.stats.txt
echo "${PctCoverage}, ${Nnonzero}, ${Ngrayordinates}" >>${OutputAtlasDenseTimeseries}_nonzero.stats.txt
# If we don't have full grayordinate coverage, save out a mask to identify those locations
if [ "$Nnonzero" -ne "$Ngrayordinates" ]; then
	${CARET7DIR}/wb_command -cifti-math 'x > 0' ${OutputAtlasDenseTimeseries}_nonzero.dscalar.nii -var x ${OutputAtlasDenseTimeseries}.stdev.dscalar.nii
fi
rm -f ${OutputAtlasDenseTimeseries}.stdev.dscalar.nii

#Basic Cleanup
rm ${VolumefMRI}_AtlasSubcortical_s${SmoothingFWHM}.nii.gz
rm ${VolumefMRI}_s${SmoothingFWHM}.atlasroi.L.${LowResMesh}k_fs_LR.func.gii
rm ${VolumefMRI}_s${SmoothingFWHM}.atlasroi.R.${LowResMesh}k_fs_LR.func.gii
