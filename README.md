# UKBB surface

A script to map UKBB fMRI data to CIFTI.  

Adapted (literally cut'n'paste) from HCP pipelines 4.3.0, specifically:
```shell
PostFreeSurfer/scripts/FreeSurfer2CaretConvertAndRegisterNonlinear.sh
fMRISurface/scripts/RibbonVolumeToSurfaceMapping.sh
fMRISurface/scripts/SurfaceSmoothing.sh
fMRISurface/scripts/SubcorticalProcessing.sh
fMRISurface/scripts/CreateDenseTimeseries.sh
```

Key steps included:
1. Convert freesurfer surfaces to gifti and warp to MNI
2. Import subcortical ROIs
3. Run MSMSulc folding-based registration to FS_LR initialized with FS affine
4. Create downsampled fs_LR (32k) surfaces
5. Ribbon constrained mapping of fMRI to surface
6. Smooth fMRI on surface
7. Sample subcortical voxels
8. Create fMRI CIFTI dense time-series


## Usage

```shell
export FREESURFER_HOME='/path/to/freesurfer'
export CARET7DIR='/path/to/workbench'
export MSMBINDIR='/path/to/msm_v3'
export HCPPIPEDIR='/path/to/hcp_pipelines'

datadir="/Users/seanf/scratch/ukbb-surf/Rsubjects6"
subid="1054688"

./ukbb_hcp_surface_mni.sh ${datadir} ${subid}

```

## Output Files

```bash
Rsubjects6/1054688
├── MNINonLinear                            # 164k_fs_LR surfaces in MNI space
│   ├── 1054688.164k_fs_LR.wb.spec
│   ├── 1054688.ArealDistortion_FS.164k_fs_LR.dscalar.nii
│   ├── 1054688.ArealDistortion_MSMSulc.164k_fs_LR.dscalar.nii
│   ├── 1054688.EdgeDistortion_FS.164k_fs_LR.dscalar.nii
│   ├── 1054688.EdgeDistortion_MSMSulc.164k_fs_LR.dscalar.nii
│   ├── 1054688.L.ArealDistortion_FS.164k_fs_LR.shape.gii
│   ├── 1054688.L.ArealDistortion_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.L.EdgeDistortion_FS.164k_fs_LR.shape.gii
│   ├── 1054688.L.EdgeDistortion_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.L.StrainJ_FS.164k_fs_LR.shape.gii
│   ├── 1054688.L.StrainJ_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.L.StrainR_FS.164k_fs_LR.shape.gii
│   ├── 1054688.L.StrainR_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.L.atlasroi.164k_fs_LR.shape.gii
│   ├── 1054688.L.curvature.164k_fs_LR.shape.gii
│   ├── 1054688.L.flat.164k_fs_LR.surf.gii
│   ├── 1054688.L.inflated.164k_fs_LR.surf.gii
│   ├── 1054688.L.midthickness.164k_fs_LR.surf.gii
│   ├── 1054688.L.pial.164k_fs_LR.surf.gii
│   ├── 1054688.L.refsulc.164k_fs_LR.shape.gii
│   ├── 1054688.L.sphere.164k_fs_LR.surf.gii
│   ├── 1054688.L.sulc.164k_fs_LR.shape.gii
│   ├── 1054688.L.thickness.164k_fs_LR.shape.gii
│   ├── 1054688.L.very_inflated.164k_fs_LR.surf.gii
│   ├── 1054688.L.white.164k_fs_LR.surf.gii
│   ├── 1054688.R.ArealDistortion_FS.164k_fs_LR.shape.gii
│   ├── 1054688.R.ArealDistortion_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.R.EdgeDistortion_FS.164k_fs_LR.shape.gii
│   ├── 1054688.R.EdgeDistortion_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.R.StrainJ_FS.164k_fs_LR.shape.gii
│   ├── 1054688.R.StrainJ_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.R.StrainR_FS.164k_fs_LR.shape.gii
│   ├── 1054688.R.StrainR_MSMSulc.164k_fs_LR.shape.gii
│   ├── 1054688.R.atlasroi.164k_fs_LR.shape.gii
│   ├── 1054688.R.curvature.164k_fs_LR.shape.gii
│   ├── 1054688.R.flat.164k_fs_LR.surf.gii
│   ├── 1054688.R.inflated.164k_fs_LR.surf.gii
│   ├── 1054688.R.midthickness.164k_fs_LR.surf.gii
│   ├── 1054688.R.pial.164k_fs_LR.surf.gii
│   ├── 1054688.R.refsulc.164k_fs_LR.shape.gii
│   ├── 1054688.R.sphere.164k_fs_LR.surf.gii
│   ├── 1054688.R.sulc.164k_fs_LR.shape.gii
│   ├── 1054688.R.thickness.164k_fs_LR.shape.gii
│   ├── 1054688.R.very_inflated.164k_fs_LR.surf.gii
│   ├── 1054688.R.white.164k_fs_LR.surf.gii
│   ├── 1054688.StrainJ_FS.164k_fs_LR.dscalar.nii
│   ├── 1054688.StrainJ_MSMSulc.164k_fs_LR.dscalar.nii
│   ├── 1054688.StrainR_FS.164k_fs_LR.dscalar.nii
│   ├── 1054688.StrainR_MSMSulc.164k_fs_LR.dscalar.nii
│   ├── 1054688.curvature.164k_fs_LR.dscalar.nii
│   ├── 1054688.sulc.164k_fs_LR.dscalar.nii
│   ├── 1054688.thickness.164k_fs_LR.dscalar.nii
│   ├── Native                              # native surfaces in MNI space
│   │   ├── 1054688.ArealDistortion_FS.native.dscalar.nii
│   │   ├── 1054688.ArealDistortion_MSMSulc.native.dscalar.nii
│   │   ├── 1054688.EdgeDistortion_FS.native.dscalar.nii
│   │   ├── 1054688.EdgeDistortion_MSMSulc.native.dscalar.nii
│   │   ├── 1054688.L.ArealDistortion_FS.native.shape.gii
│   │   ├── 1054688.L.ArealDistortion_MSMSulc.native.shape.gii
│   │   ├── 1054688.L.EdgeDistortion_FS.native.shape.gii
│   │   ├── 1054688.L.EdgeDistortion_MSMSulc.native.shape.gii
│   │   ├── 1054688.L.StrainJ_FS.native.shape.gii
│   │   ├── 1054688.L.StrainJ_MSMSulc.native.shape.gii
│   │   ├── 1054688.L.StrainR_FS.native.shape.gii
│   │   ├── 1054688.L.StrainR_MSMSulc.native.shape.gii
│   │   ├── 1054688.L.atlasroi.native.shape.gii
│   │   ├── 1054688.L.curvature.native.shape.gii
│   │   ├── 1054688.L.inflated.native.surf.gii
│   │   ├── 1054688.L.midthickness.native.surf.gii
│   │   ├── 1054688.L.pial.native.surf.gii
│   │   ├── 1054688.L.roi.native.shape.gii
│   │   ├── 1054688.L.sphere.MSMSulc.native.surf.gii
│   │   ├── 1054688.L.sphere.native.surf.gii
│   │   ├── 1054688.L.sphere.reg.native.surf.gii
│   │   ├── 1054688.L.sphere.reg.reg_LR.native.surf.gii
│   │   ├── 1054688.L.sphere.rot.native.surf.gii
│   │   ├── 1054688.L.sulc.native.shape.gii
│   │   ├── 1054688.L.thickness.native.shape.gii
│   │   ├── 1054688.L.very_inflated.native.surf.gii
│   │   ├── 1054688.L.white.native.surf.gii
│   │   ├── 1054688.R.ArealDistortion_FS.native.shape.gii
│   │   ├── 1054688.R.ArealDistortion_MSMSulc.native.shape.gii
│   │   ├── 1054688.R.EdgeDistortion_FS.native.shape.gii
│   │   ├── 1054688.R.EdgeDistortion_MSMSulc.native.shape.gii
│   │   ├── 1054688.R.StrainJ_FS.native.shape.gii
│   │   ├── 1054688.R.StrainJ_MSMSulc.native.shape.gii
│   │   ├── 1054688.R.StrainR_FS.native.shape.gii
│   │   ├── 1054688.R.StrainR_MSMSulc.native.shape.gii
│   │   ├── 1054688.R.atlasroi.native.shape.gii
│   │   ├── 1054688.R.curvature.native.shape.gii
│   │   ├── 1054688.R.inflated.native.surf.gii
│   │   ├── 1054688.R.midthickness.native.surf.gii
│   │   ├── 1054688.R.pial.native.surf.gii
│   │   ├── 1054688.R.roi.native.shape.gii
│   │   ├── 1054688.R.sphere.MSMSulc.native.surf.gii
│   │   ├── 1054688.R.sphere.native.surf.gii
│   │   ├── 1054688.R.sphere.reg.native.surf.gii
│   │   ├── 1054688.R.sphere.reg.reg_LR.native.surf.gii
│   │   ├── 1054688.R.sphere.rot.native.surf.gii
│   │   ├── 1054688.R.sulc.native.shape.gii
│   │   ├── 1054688.R.thickness.native.shape.gii
│   │   ├── 1054688.R.very_inflated.native.surf.gii
│   │   ├── 1054688.R.white.native.surf.gii
│   │   ├── 1054688.StrainJ_FS.native.dscalar.nii
│   │   ├── 1054688.StrainJ_MSMSulc.native.dscalar.nii
│   │   ├── 1054688.StrainR_FS.native.dscalar.nii
│   │   ├── 1054688.StrainR_MSMSulc.native.dscalar.nii
│   │   ├── 1054688.curvature.native.dscalar.nii
│   │   ├── 1054688.native.wb.spec
│   │   ├── 1054688.sulc.native.dscalar.nii
│   │   ├── 1054688.thickness.native.dscalar.nii
│   │   └── MSMSulc
│   │       ├── L.logdir
│   │       │   ├── MSM.log
│   │       │   └── conf
│   │       ├── L.mat
│   │       ├── L.sphere.LR.reg.surf.gii
│   │       ├── L.sphere.reg.surf.gii
│   │       ├── L.sphere_rot.surf.gii
│   │       ├── L.transformed_and_reprojected.func.gii
│   │       ├── R.logdir
│   │       │   ├── MSM.log
│   │       │   └── conf
│   │       ├── R.mat
│   │       ├── R.sphere.LR.reg.surf.gii
│   │       ├── R.sphere.reg.surf.gii
│   │       ├── R.sphere_rot.surf.gii
│   │       └── R.transformed_and_reprojected.func.gii
│   ├── ROIs
│   │   ├── Atlas_ROIs.2.nii.gz
│   │   ├── Atlas_wmparc.2.nii.gz
│   │   ├── ROIs.2.nii.gz
│   │   └── wmparc.2.nii.gz
│   ├── Results
│   │   └── filtered_func_data_clean
│   │       ├── filtered_func_data_clean_Atlas.dtseries.nii  # fMRI CIFTI dtseries
│   │       ├── filtered_func_data_clean_Atlas_mean.dscalar.nii
│   │       └── filtered_func_data_clean_Atlas_nonzero.stats.txt
│   ├── T1.2.nii.gz
│   ├── T1.nii.gz
│   ├── T1_brain_mask.nii.gz
│   ├── aparc+aseg.nii.gz
│   ├── aparc.a2009s+aseg.nii.gz
│   ├── fsaverage                       
│   │   ├── 1054688.L.def_sphere.164k_fs_L.surf.gii
│   │   ├── 1054688.L.sphere.164k_fs_L.surf.gii
│   │   ├── 1054688.R.def_sphere.164k_fs_R.surf.gii
│   │   └── 1054688.R.sphere.164k_fs_R.surf.gii
│   ├── fsaverage_LR32k                     # 32k_fs_LR surfaces in MNI space
│   │   ├── 1054688.32k_fs_LR.wb.spec
│   │   ├── 1054688.ArealDistortion_FS.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.ArealDistortion_MSMSulc.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.EdgeDistortion_FS.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.EdgeDistortion_MSMSulc.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.L.ArealDistortion_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.ArealDistortion_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.EdgeDistortion_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.EdgeDistortion_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.StrainJ_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.StrainJ_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.StrainR_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.StrainR_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.atlasroi.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.curvature.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.flat.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.midthickness.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.pial.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.sphere.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.sulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.thickness.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.very_inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.white.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.ArealDistortion_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.ArealDistortion_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.EdgeDistortion_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.EdgeDistortion_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.StrainJ_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.StrainJ_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.StrainR_FS.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.StrainR_MSMSulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.atlasroi.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.curvature.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.flat.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.midthickness.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.pial.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.sphere.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.sulc.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.thickness.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.very_inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.white.32k_fs_LR.surf.gii
│   │   ├── 1054688.StrainJ_FS.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.StrainJ_MSMSulc.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.StrainR_FS.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.StrainR_MSMSulc.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.curvature.32k_fs_LR.dscalar.nii
│   │   ├── 1054688.sulc.32k_fs_LR.dscalar.nii
│   │   └── 1054688.thickness.32k_fs_LR.dscalar.nii
│   └── wmparc.nii.gz
├── RibbonVolumeToSurfaceMapping                # Ribbon constrained mapping files
│   ├── L.cov.32k_fs_LR.func.gii
│   ├── L.cov.native.func.gii
│   ├── L.cov_all.32k_fs_LR.func.gii
│   ├── L.cov_all.native.func.gii
│   ├── L.goodvoxels.32k_fs_LR.func.gii
│   ├── L.goodvoxels.native.func.gii
│   ├── L.mean.32k_fs_LR.func.gii
│   ├── L.mean.native.func.gii
│   ├── L.mean_all.32k_fs_LR.func.gii
│   ├── L.mean_all.native.func.gii
│   ├── R.cov.32k_fs_LR.func.gii
│   ├── R.cov.native.func.gii
│   ├── R.cov_all.32k_fs_LR.func.gii
│   ├── R.cov_all.native.func.gii
│   ├── R.goodvoxels.32k_fs_LR.func.gii
│   ├── R.goodvoxels.native.func.gii
│   ├── R.mean.32k_fs_LR.func.gii
│   ├── R.mean.native.func.gii
│   ├── R.mean_all.32k_fs_LR.func.gii
│   ├── R.mean_all.native.func.gii
│   ├── SmoothNorm.nii.gz
│   ├── cov.nii.gz
│   ├── cov_norm_modulate.nii.gz
│   ├── cov_norm_modulate_ribbon.nii.gz
│   ├── cov_ribbon.nii.gz
│   ├── cov_ribbon_norm.nii.gz
│   ├── cov_ribbon_norm_s5.nii.gz
│   ├── goodvoxels.nii.gz
│   ├── mask.nii.gz
│   ├── mean.nii.gz
│   ├── ribbon_only.nii.gz
│   └── std.nii.gz
├── T1
│   ├── Native                                # native surfaces in T1w space
│   │   ├── 1054688.L.inflated.native.surf.gii
│   │   ├── 1054688.L.midthickness.native.surf.gii
│   │   ├── 1054688.L.pial.native.surf.gii
│   │   ├── 1054688.L.very_inflated.native.surf.gii
│   │   ├── 1054688.L.white.native.surf.gii
│   │   ├── 1054688.R.inflated.native.surf.gii
│   │   ├── 1054688.R.midthickness.native.surf.gii
│   │   ├── 1054688.R.pial.native.surf.gii
│   │   ├── 1054688.R.very_inflated.native.surf.gii
│   │   ├── 1054688.R.white.native.surf.gii
│   │   └── 1054688.native.wb.spec
│   ├── T1.nii.gz
│   ├── T1_brain_mask.nii.gz
│   ├── aparc+aseg.nii.gz
│   ├── aparc+aseg_1mm.nii.gz
│   ├── aparc.a2009s+aseg.nii.gz
│   ├── aparc.a2009s+aseg_1mm.nii.gz
│   ├── fsaverage_LR32k                     # 32k_fs_LR surfaces in T1w space
│   │   ├── 1054688.32k_fs_LR.wb.spec
│   │   ├── 1054688.L.inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.midthickness.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.midthickness_va.32k_fs_LR.shape.gii
│   │   ├── 1054688.L.pial.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.very_inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.L.white.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.midthickness.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.midthickness_va.32k_fs_LR.shape.gii
│   │   ├── 1054688.R.pial.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.very_inflated.32k_fs_LR.surf.gii
│   │   ├── 1054688.R.white.32k_fs_LR.surf.gii
│   │   ├── 1054688.midthickness_va.32k_fs_LR.dscalar.nii
│   │   └── 1054688.midthickness_va_norm.32k_fs_LR.dscalar.nii
│   ├── transforms
│   │   ├── MNI_to_T1_warp.nii.gz
│   │   └── T1_to_MNI_warp.nii.gz
│   ├── wmparc.nii.gz
│   └── wmparc_1mm.nii.gz
└── fMRI
    └── rfMRI.ica
        ├── example_func.nii.gz
        ├── filtered_func_data.nii.gz
        ├── reg
        │   ├── example_func2highres.mat
        │   ├── example_func2standard_warp.nii.gz
        │   ├── highres.nii.gz
        │   └── highres2example_func.mat
        └── reg_standard
            ├── filtered_func_data_clean.L.native.func.gii
            ├── filtered_func_data_clean.R.native.func.gii
            ├── filtered_func_data_clean.nii.gz
            ├── filtered_func_data_clean_stdmask.nii.gz
            └── filtered_func_data_mean.nii.gz
```
